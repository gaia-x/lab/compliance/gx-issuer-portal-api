import { Test, TestingModule } from '@nestjs/testing'
import { CredentialsService } from './credentials.service'

describe('CredentialsController', () => {
  let controller: CredentialsContainer

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CredentialsContainer],
      providers: [CredentialsService]
    }).compile()

    controller = module.get<CredentialsContainer>(CredentialsContainer)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})
