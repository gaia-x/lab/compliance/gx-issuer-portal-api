import { Controller, Post, Body } from '@nestjs/common'
import { Throttle } from '@nestjs/throttler'
import { CredentialsService } from './credentials.service'
import { IssueCredentialsDto } from './dto/IssueCredential.dto'
import { IssueCredentialValidatorPipe } from './validator/IssueCredentialValidatorPipe'

@Controller('credentials')
export class CredentialsController {
  constructor(private readonly credentialsService: CredentialsService) { }

  @Throttle(5, 60)
  @Post('/issue')
  issue(@Body(new IssueCredentialValidatorPipe()) issueCredentialsDto: IssueCredentialsDto) {
    return this.credentialsService.issue(issueCredentialsDto)
  }
}
