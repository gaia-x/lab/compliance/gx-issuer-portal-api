

import { Module } from '@nestjs/common'
import { APP_GUARD } from '@nestjs/core'
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler'
import { MailsModule } from '../mails/mails.module'
import { CredentialsController } from './credentials.controller'
import { CredentialsService } from './credentials.service'

@Module({
  imports: [
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 5
    }),
    MailsModule
  ],
  controllers: [CredentialsController],
  providers: [CredentialsService, {
    provide: APP_GUARD,
    useClass: ThrottlerGuard
  }]
})
export class CredentialsModule { }
