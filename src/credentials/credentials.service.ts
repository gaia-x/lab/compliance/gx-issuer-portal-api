import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common'
import { pbkdf2Sync } from 'crypto'
import fetch from 'node-fetch'
import { MailsService } from '../mails/services/mails.service'
import { TIssueCredentialParams, IssueCredentialsDto } from './dto/IssueCredential.dto'

@Injectable()
export class CredentialsService {
  issuerAuthCredentials = {
    id: CredentialsService.hashValueWithSalt(process.env.CREDENTIAL_ISSUER_ID),
    password: CredentialsService.hashValueWithSalt(process.env.CREDENTIAL_ISSUER_PASSWORD)
  }
  issuerBaseUri = 'https://wallet.lab.gaia-x.eu'

  private readonly logger = new Logger(CredentialsService.name)

  constructor(
    private readonly mailsService: MailsService
  ){ }

  async issue({ credentials, params, onboardingToken }: IssueCredentialsDto): Promise<{ redirectUrl: string }> {
    const result = await this.mailsService.isValidLink(onboardingToken)
    if((result as any).isValidLink){
      const issuerToken = await this.loginIssuer()
    
      const redirectUrl = await this.issueCredentials(credentials, params, issuerToken)
  
      return { redirectUrl }
    }
  }

  private async issueCredentials(credentials: IssueCredentialsDto['credentials'], params: TIssueCredentialParams, token: string): Promise<string> {
    const url = new URL('/issuer-api/credentials/issuance/request', this.issuerBaseUri)
    
    // remove empty values from search query
    Object.keys(params).forEach(key => params[key] === undefined && delete params[key])
    const query = new URLSearchParams(params)
    
    const requestOptions = {
      method: 'POST',
      headers: { Authorization: `Bearer ${token}` },
      body: JSON.stringify({ credentials })
    }
    
    try {
      const href = `${url}?${query}`
      const response = await fetch(href, requestOptions)
      const result = await response.text()
      this.logger.debug('Issuing credentials on url with requestOptions:', { href, requestOptions, result })
      return result
    } catch(error) {
      this.logger.error(error)
      throw new InternalServerErrorException()
    }

  }

  private async loginIssuer(): Promise<string> {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(this.issuerAuthCredentials)
    }

    try {
      const url = new URL('/api/auth/login', this.issuerBaseUri)
      this.logger.debug('Authenticating with issuer:', { url, requestOptions })
      const response = await fetch(url, requestOptions)
      const { token } = await response.json()
      this.logger.debug('Got auth token from issuer:', { token })
      return token
    } catch (error) {
      this.logger.error(error)
      throw new InternalServerErrorException()
    }
  }

  static hashValueWithSalt(value: string): string {
    return pbkdf2Sync(value, process.env.SALT, 1000, 64, 'sha512').toString('hex')
  }
}
