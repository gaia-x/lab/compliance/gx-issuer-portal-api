export interface ICredentialSubject {
    hasLegallyBindingName: string,
    hasRegistrationNumber: string,
    hasCountry: string,
    id: string,
    hasJurisdiction: string,
    leiCode?: string,
    parentOrganisation?: string
    subOrganisation?: string,
    ethereumAddress?: string
}

export type TIssueCredentialParams = {
    walletId: string
    sessionId?: string
}
export class IssueCredentialsDto {
    credentials: {
        credentialData: {
            credentialSubject: ICredentialSubject
        },
        schemaId: string,
        type: string
    }[]
    onboardingToken: string
    params: TIssueCredentialParams
}