import { ICredentialSubject } from './IssueCredential.dto'

export class IssueCredentialRequestDto {
    credentialSubject: ICredentialSubject
    onboardingToken: string
    walletId?: string
    sessionId?: string
}
