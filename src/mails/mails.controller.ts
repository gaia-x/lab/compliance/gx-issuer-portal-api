import { Controller, Get, Post, Body, Param } from '@nestjs/common'
import { MailsService } from './services/mails.service'
import { CreateMailDto } from './dto/create-mail.dto'
import { Throttle } from '@nestjs/throttler'

@Controller('mails')
export class MailsController {
  constructor(private readonly mailsService: MailsService) { }

  @Throttle(30, 60)
  @Post()
  create(@Body() createMailDto: CreateMailDto) {
    return this.mailsService.create(createMailDto)
  }

  @Get(':id')
  isValidLink(@Param('id') id: string) {
    return this.mailsService.isValidLink(id)
  }
}
