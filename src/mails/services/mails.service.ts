import { Injectable, InternalServerErrorException, ForbiddenException, Logger } from '@nestjs/common'
import { CreateMailDto } from '../dto/create-mail.dto'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { pbkdf2Sync } from 'crypto'
import { v4 as uuidv4, validate as uuidValidate } from 'uuid'

import { registration_record } from '../entities/registration.entity'
import { SendMailService } from './send-mail.service'

@Injectable()
export class MailsService {
  constructor(
    private readonly sendMailService: SendMailService,
    @InjectRepository(registration_record, 'RegistrationConnection')
    private readonly registrationRepository: Repository<registration_record>,
  ) { }

  private readonly logger = new Logger(MailsService.name)

  async create(createMailDto: CreateMailDto): Promise<any> { // TODO change any to specific ojbect
    const { email, sessionId } = createMailDto

    try {
      this.logger.log('Creating new token.')

      const salt = process.env.SALT
      const email_hash = this.hashEmail(email, salt)
      const token = uuidv4()
      const expires_at = new Date(Date.now() + (3600 * 1000 * 24)) // add one day

      // entry is either updated or created it if it does not exist
      await this.storeEntry({ email_hash, token, expires_at })

      this.logger.log('Attempting to send registration email.')
      const options = this.sendMailService.createRegistrationMailOptions(email, token, sessionId)
      await this.sendMailService.send(this.sendMailService.mailTemplate, options)

      // always send this message to avoid someone using this API to identify available emails
      return { message: 'A new registration link was sent to ' + email }
    } catch (e) {
      this.logger.error(e)
      throw new InternalServerErrorException()
    }
  }

  private async storeEntry(user) {
    const { email_hash, ...update } = user

    const result = await this.registrationRepository.findOne({ email_hash })

    const updatedEntry = result
      ? await this.registrationRepository.update({ email_hash }, update)
      : await this.registrationRepository.save(user)

    return updatedEntry
  }

  private hashEmail(email, salt) {
    return pbkdf2Sync(email, salt, 1000, 64, 'sha512').toString('hex')
  }

  async isValidLink(uuid: string): Promise<any> {
    const isValidUuid = this.validateId(uuid)

    if (!isValidUuid) {
      throw new ForbiddenException()
    }

    const result = await this.registrationRepository.findOne({ token: uuid })

    // if expiresAt is undefined the result would be false that's why we use the nullish operator here
    if (!result || (result.expires_at ?? 0) < Date.now()) {
      // entries are no longer needed in database
      await this.registrationRepository.delete({ token: uuid })
      throw new ForbiddenException()
    }

    return { isValidLink: true }
  }

  private validateId(id) {
    const isValidId = uuidValidate(id)

    return isValidId
  }
}
