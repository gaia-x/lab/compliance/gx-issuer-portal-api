import { Injectable, InternalServerErrorException, Logger } from '@nestjs/common'
import * as nodemailer from 'nodemailer'
import Email from 'email-templates'
import path from 'path'

@Injectable()
export class SendMailService {
  private transporter: nodemailer.Transporter

  private readonly logger = new Logger(SendMailService.name)

  public readonly mailTemplate: Email

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: Number(process.env.MAIL_PORT),
      secure: true,
      auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
      }
    })

    this.mailTemplate = new Email({
      views: {
        root: path.join(__dirname, '../templates')
      },
      juiceResources: {
        webResources: {
          relativeTo: path.join(__dirname, '../templates/assets')
        } 
      },
      preview: true,
      transport: this.transporter,
      message: {
        from: process.env.FROM_EMAIL
      }
    })
  }

  async send(template: Email, options?: Email.EmailOptions): Promise<any> {
    try {
      const info = await template.send(options)

      this.logger.log('Email succesfully sent.')

      return info
    } catch (e) {
      this.logger.error(e)
      throw new InternalServerErrorException()
    }
  }

  createRegistrationMailOptions(to: string, uuid: string, sessionId?: string): Email.EmailOptions {
    const url = new URL('https://onboarding-portal.lab.gaia-x.eu/issuance')
    url.searchParams.set('activation_link', uuid)
    sessionId && url.searchParams.set('sessionId', sessionId)

    const options = {
      template: 'registration',
      message: {
        to: to
      },
      locals: {
        activationLink: url.href
      }
    }

    return options
  }
}
